import logging

from heapq import nlargest
from random import randint, random
from time import time

from algorithms.genetic_phases.crossover import CROSSOVER_METHODS
from algorithms.genetic_phases.mutation import MUTATION_METHODS
from algorithms.genetic_phases.selection import SELECTION_METHODS


class GeneticAlgorithm:
    def __init__(
            self, individual, population_size=100, nb_generations=100,
            tournament_size=5, ranking_size=20, random_reproduction_size=0,
            reproduction_size=50, children_size=50, mutation_probability=.01,
            log_interval=10, visualization=False, timeout=0,
            selection_type='tournament', crossover_type='ox', mutation_type='swap'
    ):

        self.population_size = population_size
        self.reproduction_size = reproduction_size
        self.random_reproduction_size = random_reproduction_size
        self.children_size = children_size
        self.nb_generations = nb_generations
        self.tournament_size = tournament_size
        self.ranking_size = ranking_size
        self.mutation_probability = mutation_probability

        self.log_interval = log_interval
        self.visualization = visualization
        self.timeout = timeout
        self.selection_type = selection_type
        self.crossover_type = crossover_type
        self.mutation_type = mutation_type

        self.individual = individual

        self.reproduction = []
        self.population = []
        for i in range(self.population_size):
            self.population.append(individual.copy())

        self.generation = 1
        self.start_time = 0
        self.best_individual_history = {}

    @property
    def logger(self):
        return logging.getLogger(__name__)

    def crossover(self):
        for _ in range(self.children_size):
            i1 = randint(0, len(self.reproduction) - 2)
            i2 = randint(0, len(self.reproduction) - 1)
            if i2 <= i1:
                i1, i2 = i2, i1 + 1

            child = CROSSOVER_METHODS[self.crossover_type](self.reproduction[i1],
                                                           self.reproduction[i2])
            self.population.append(child)

    def mutation(self):
        for individual in self.population:
            if random() < self.mutation_probability:
                MUTATION_METHODS[self.mutation_type](individual)

    def selection(self):
        self.reproduction = SELECTION_METHODS[self.selection_type](self)

    def evolution(self):
        for individual in self.population:
            individual.calculate_fitness()

        self.population = nlargest(self.population_size,
                                   self.population,
                                   key=lambda x: x.fitness)

    def initialise(self):
        for individual in self.population:
            individual.init_genome()

    def best_individual(self):
        return max(self.population, key=lambda x: x.fitness)

    def random_reproduction(self):
        for _ in range(self.random_reproduction_size):
            individual = self.individual.copy(shuffle_genome=True)
            self.reproduction.append(individual)

    def run(self):
        self.log_start_info()

        if self.generation == 1:
            self.initialise()
            self.evolution()

        try:
            while True:
                self.selection()
                self.random_reproduction()
                self.crossover()
                self.mutation()
                self.evolution()

                self.log_info()

                if self.break_loop():
                    break
        except KeyboardInterrupt:
            self.logger.info(f'Keyboard Interrupt')

        return self.best_individual()

    def log_start_info(self):
        self.start_time = time()

        self.logger.info(f'Population size: {self.population_size}')
        self.logger.info(f'Number of generations: {self.nb_generations}')
        self.logger.info(f'Tournament size: {self.tournament_size}')
        self.logger.info(f'Ranking size: {self.ranking_size}')
        self.logger.info(f'Reproduction size: {self.reproduction_size}')
        self.logger.info(f'Random reproduction size: {self.random_reproduction_size}')
        self.logger.info(f'Children size: {self.children_size}')
        self.logger.info(f'Mutation probability: {self.mutation_probability}')
        self.logger.info(f'Log interval: {self.log_interval}')
        self.logger.info(f'Visualization: {self.visualization}')
        self.logger.info(f'Selection type: {self.selection_type}')
        self.logger.info(f'Crossover type: {self.crossover_type}')
        self.logger.info(f'Mutation type: {self.mutation_type}')
        self.logger.info('-' * 50)

    def log_info(self):
        if self.generation in (1, self.nb_generations) or self.generation % self.log_interval == 0:
            bi = self.best_individual()
            self.best_individual_history[self.generation] = bi.copy(clear_knapsack=False)

            t = (time() - self.start_time) / 60
            info = f'Generation: {self.generation}, Time: {t:.2f} min, Fitness: {bi.fitness:.5f}'
            self.logger.info(info)

            if self.visualization:
                pass

    def break_loop(self):
        self.generation += 1
        if self.nb_generations and self.generation > self.nb_generations:
            self.logger.info(f'Generation limit ({self.nb_generations})')
            return True
        if self.timeout and time() - self.start_time > self.timeout * 60:
            self.logger.info(f'Timeout ({self.timeout})')
            return True


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

    from algorithms.individuals.travelling_thief import TravellingThief
    from models.ttp import TravellingThiefProblem
    from visualization.visualization import show_route

    directory = r'/drive/Projects/computer-science/' \
                r'artificial-intelligence-and-knowledge-engineering/lab1/ai-lab1-ttp_data/'
    file_name = 'hard_1.ttp'

    path = directory + file_name
    model = TravellingThiefProblem(path)
    travelling_thief = TravellingThief(model)

    ga = GeneticAlgorithm(travelling_thief)
    thief = ga.run()

    print()
    print('Genome', thief.genome)
    print('Fitness', thief.fitness)

    show_route(thief)
