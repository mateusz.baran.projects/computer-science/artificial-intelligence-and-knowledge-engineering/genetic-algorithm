from random import randint


def crossover_ox(father, mother):
    i1 = randint(0, len(father.genome) - 1)
    i2 = randint(0, len(father.genome) - 1)
    if i2 < i1:
        i1, i2 = i2, i1

    locus = father.genome[i1:i2]
    child_genome = [x for x in mother.genome if x not in locus]
    child_genome[i1:i1] = locus

    child = father.copy(genome=child_genome)

    return child


def crossover_pmx(father, mother):
    i1 = randint(0, len(father.genome) - 1)
    i2 = randint(0, len(father.genome) - 1)
    if i2 < i1:
        i1, i2 = i2, i1

    dictionary = {}

    father_before = father.genome[:i1]
    father_after = father.genome[i2:]
    father_locus = father.genome[i1:i2]
    mother_locus = mother.genome[i1:i2]

    for i in range(i2 - i1):
        mother_gen = mother_locus[i]
        if mother_gen not in father_locus:
            father_gen = father_locus[i]
            while father_gen in mother_locus:
                index = mother_locus.index(father_gen)
                father_gen = father_locus[index]

            dictionary[mother_gen] = father_gen

    for i in range(len(father_before)):
        if father_before[i] in dictionary:
            father_before[i] = dictionary[father_before[i]]

    for i in range(len(father_after)):
        if father_after[i] in dictionary:
            father_after[i] = dictionary[father_after[i]]

    child_genome = father_before + mother_locus + father_after

    child = father.copy(genome=child_genome)

    return child


CROSSOVER_METHODS = {
    'ox': crossover_ox,
    'pmx': crossover_pmx,
}
