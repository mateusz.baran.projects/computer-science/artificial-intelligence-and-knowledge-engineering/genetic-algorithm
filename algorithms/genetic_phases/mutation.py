from random import randint


def mutation_swap(individual):
    i1 = randint(0, len(individual.genome) - 2)
    i2 = randint(0, len(individual.genome) - 1)
    if i2 <= i1:
        i1, i2 = i2, i1 + 1

    temp = individual.genome[i1]
    individual.genome[i1] = individual.genome[i2]
    individual.genome[i2] = temp
    individual.clear_knapsack()


def mutation_inverse(individual):
    i1 = randint(0, len(individual.genome) - 2)
    i2 = randint(0, len(individual.genome) - 1)
    if i2 <= i1:
        i1, i2 = i2, i1 + 1

    temp = individual.genome[i1:i2]
    individual.genome[i1:i2] = temp[::-1]
    individual.clear_knapsack()


MUTATION_METHODS = {
    'swap': mutation_swap,
    'inverse': mutation_inverse,
}
