from random import random, sample


def selection_roulette_wheel(self):

    def random_index():
        r = random()
        for i, p in enumerate(wheel):
            if p > r:
                return i - 1
        return len(wheel) - 1

    min_total_fitness = 1 / len(self.population)
    min_fitness = min(self.population, key=lambda x: x.fitness).fitness
    total_fitness = sum([x.fitness for x in self.population]) + min_fitness * len(self.population)
    total_fitness = total_fitness if total_fitness > min_total_fitness else min_total_fitness

    wheel = []
    probability = 0
    for individual in self.population:
        probability += (individual.fitness - min_fitness) / total_fitness
        wheel.append(probability)

    rep_pop = []
    for _ in range(self.reproduction_size):
        rep_pop.append(self.population[random_index()])

    return rep_pop


def selection_tournament(self):
    rep_pop = []
    for _ in range(self.reproduction_size):
        best = max(sample(self.population, self.tournament_size), key=lambda x: x.fitness)
        rep_pop.append(best)

    return rep_pop


def selection_ranking(self):
    rep_pop = sorted(self.population, key=lambda x: x.fitness, reverse=True)[:self.ranking_size]
    return rep_pop


SELECTION_METHODS = {
    'roulette_wheel': selection_roulette_wheel,
    'tournament': selection_tournament,
    'ranking': selection_ranking,
}
