class Individual:
    def __init__(self):
        self.genome = []
        self.fitness = 0

    def init_genome(self):
        raise NotImplementedError

    def calculate_fitness(self):
        raise NotImplementedError

    def copy(self):
        raise NotImplementedError
