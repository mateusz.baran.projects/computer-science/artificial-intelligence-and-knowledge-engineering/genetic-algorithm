from random import shuffle

from algorithms.individuals.individual import Individual


class TravellingThief(Individual):
    def __init__(self, ttp_model=None, greedy_algorithm='ratio'):
        super().__init__()

        self.time = 0
        self.weight_of_knapsack = 0
        self.value_of_knapsack = 0

        if ttp_model is not None:
            self.greedy_algorithm = greedy_algorithm
            self.model = ttp_model
            self.genome = list(self.model.nodes)

    def init_genome(self):
        shuffle(self.genome)

    def clear_knapsack(self):
        self.time = 0
        self.weight_of_knapsack = 0
        self.value_of_knapsack = 0

    def add_item_to_knapsack(self, node):
        if node in self.model.greedy_algorithms[self.greedy_algorithm]:
            item = self.model.greedy_algorithms[self.greedy_algorithm][node]
            if item['WEIGHT'] + self.weight_of_knapsack <= self.model.capacity_of_knapsack:
                self.weight_of_knapsack += item['WEIGHT']
                self.value_of_knapsack += item['PROFIT']

    def get_velocity(self):
        return (self.model.v_max - self.weight_of_knapsack * self.model.v_diff /
                self.model.capacity_of_knapsack)

    def _f(self, i):
        self.add_item_to_knapsack(self.genome[i])
        distance = self.model.distance_matrix[self.genome[i]][self.genome[i + 1]]
        self.time += distance / self.get_velocity()
        # self.log_info(i, self.time, distance)

    def f(self):
        for i in range(len(self.genome) - 1):
            self._f(i)
        self._f(-1)

        return self.time

    def log_info(self, i, time, distance):
        print(f'City: {self.genome[i]}')
        print(f'Time: {time}')
        print(f'Distance: {distance}')
        print(f'Velocity: {self.get_velocity()}')
        print(f'Weight: {self.weight_of_knapsack}')
        print(f'Value: {self.value_of_knapsack}')
        print('-' * 50)

    def g(self):
        return self.value_of_knapsack

    def calculate_fitness(self):
        f = self.time if self.time else self.f()
        self.fitness = self.g() - f

    def copy(self, genome=None, clear_knapsack=True, shuffle_genome=False):
        travelling_thief = TravellingThief()
        travelling_thief.model = self.model
        travelling_thief.genome = self.genome[:] if genome is None else genome
        travelling_thief.fitness = self.fitness
        travelling_thief.greedy_algorithm = self.greedy_algorithm

        if not clear_knapsack:
            travelling_thief.time = self.time
            travelling_thief.weight_of_knapsack = self.weight_of_knapsack
            travelling_thief.value_of_knapsack = self.value_of_knapsack

        if shuffle_genome:
            self.init_genome()

        return travelling_thief


if __name__ == '__main__':
    from models.ttp import TravellingThiefProblem
    from visualization.visualization import show_route

    directory = r'/drive/Projects/computer-science/' \
                r'artificial-intelligence-and-knowledge-engineering/lab1/ai-lab1-ttp_data/'
    file_name = 'trivial_1.ttp'

    path = directory + file_name
    model = TravellingThiefProblem(path)

    individual = TravellingThief(model)
    individual.genome = [2, 1, 5, 6, 4, 10, 9, 8, 3, 7]
    individual.calculate_fitness()
    print('Fitness', individual.fitness)

    show_route(individual)
