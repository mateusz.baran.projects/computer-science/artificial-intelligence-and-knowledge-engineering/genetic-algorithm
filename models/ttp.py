import csv


class TravellingThiefProblem:
    def __init__(self, path):
        self.nodes = {}
        self.distance_matrix = {}
        self.items = {}
        self.assignment_items = {}
        self.greedy_algorithms = {}

        with open(path, newline='') as file:
            self.model = list(csv.reader(file, delimiter='\t'))
            self._init()
            self._init_distance_matrix()
            self._init_assignments()
            self._init_greedy_algorithm()

    def _init(self):
        self.name = self.model[0][1]
        self.dimension = int(self.model[2][1])
        self.nb_items = int(self.model[3][1])
        self.capacity_of_knapsack = int(self.model[4][1])
        self.v_min = float(self.model[5][1])
        self.v_max = float(self.model[6][1])
        self.v_diff = self.v_max - self.v_min

        for row in self.model[10:10 + self.dimension]:
            self.nodes[int(row[0])] = {
                'INDEX': int(row[0]),
                'X': float(row[1]),
                'Y': float(row[2])
            }

        for row in self.model[11 + self.dimension:]:
            self.items[int(row[0])] = {
                'INDEX': int(row[0]),
                'PROFIT': float(row[1]),
                'WEIGHT': float(row[2]),
                'ASSIGNMENT': int(row[3])
            }

    @staticmethod
    def distance(x1, y1, x2, y2):
        return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** .5

    def _init_distance_matrix(self):
        for k1, v1 in self.nodes.items():
            for k2, v2 in self.nodes.items():
                x1, y1 = v1['X'], v1['Y']
                x2, y2 = v2['X'], v2['Y']

                distance = self.distance(x1, y1, x2, y2)

                if k1 not in self.distance_matrix:
                    self.distance_matrix[k1] = {}

                if k2 not in self.distance_matrix:
                    self.distance_matrix[k2] = {}

                self.distance_matrix[k1][k2] = distance
                self.distance_matrix[k2][k1] = distance

    def _init_assignments(self):
        for k, v in self.items.items():
            if v['ASSIGNMENT'] not in self.assignment_items:
                self.assignment_items[v['ASSIGNMENT']] = [v]
            else:
                self.assignment_items[v['ASSIGNMENT']].append(v)

    def _init_greedy_algorithm(self):
        greedy_weight = {}
        greedy_profit = {}
        greedy_ratio = {}

        for k, v in self.assignment_items.items():
            best_weight = 0
            best_weight_item = None
            best_profit = 0
            best_profit_item = None
            best_ratio = 0
            best_ratio_item = None

            for item in v:
                weight = item['WEIGHT']
                profit = item['PROFIT']
                ratio = profit / weight

                if best_weight_item is None or weight < best_weight:
                    best_weight = weight
                    best_weight_item = item

                if best_profit_item is None or profit > best_profit:
                    best_profit = profit
                    best_profit_item = item

                if best_ratio_item is None or ratio > best_ratio:
                    best_ratio = ratio
                    best_ratio_item = item

            greedy_weight[k] = best_weight_item
            greedy_profit[k] = best_profit_item
            greedy_ratio[k] = best_ratio_item

        self.greedy_algorithms['weight'] = greedy_weight
        self.greedy_algorithms['profit'] = greedy_profit
        self.greedy_algorithms['ratio'] = greedy_ratio
