import logging
import os
import pickle as pkl
from datetime import datetime

import numpy as np

from algorithms.genetic_algorithm import GeneticAlgorithm
from algorithms.individuals.travelling_thief import TravellingThief
from models.ttp import TravellingThiefProblem
from visualization.visualization import show_learning_plot


class TTPRunner:
    def __init__(self, name, model_config, individual_config, genetic_algorithm_config, nb_runs=5):
        self.timestamp = datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
        self.name = name
        self.version = 1
        self.iteration = 0

        self.model_config = model_config
        self.individual_config = individual_config
        self.genetic_algorithm_config = genetic_algorithm_config
        self.nb_runs = nb_runs

        self._init()

    @property
    def logger(self):
        return logging.getLogger(__name__)

    def _init(self):
        self.model = TravellingThiefProblem(**self.model_config)
        self.individual = TravellingThief(ttp_model=self.model, **self.individual_config)

        self.genetic_algorithms = []
        for i in range(self.nb_runs):
            self.genetic_algorithms.append(GeneticAlgorithm(individual=self.individual,
                                                            **self.genetic_algorithm_config))

    def run(self, verbose=True):
        if verbose:
            logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

        self.iteration = self.iteration % self.nb_runs
        try:
            while self.iteration < self.nb_runs:
                self.logger.info('-' * 50)
                self.logger.info('-' * 10 + f' Model: {self.name}')
                self.logger.info('-' * 10 + f' Iteration: {self.iteration + 1} / {self.nb_runs}')
                self.logger.info('-' * 50)

                self.genetic_algorithms[self.iteration].run()
                self.iteration += 1

        except KeyboardInterrupt:
            self.logger.info(f'Keyboard Interrupt')

    def fitness_history(self):
        nb_gen_histories = []
        fitness_histories = []

        for ga in self.genetic_algorithms:
            fitness_history = list(map(lambda x: x.fitness,
                                       list(ga.best_individual_history.values())))
            fitness_histories.append(fitness_history)

            nb_gen_history = list(ga.best_individual_history.keys())
            nb_gen_histories.append(nb_gen_history)

        nb_gen = max(nb_gen_histories, key=lambda x: len(x))
        temp = np.ones(shape=(len(fitness_histories), len(nb_gen)))
        for i in range(len(fitness_histories)):
            temp[i][:len(fitness_histories[i])] = fitness_histories[i]
            temp[i][len(fitness_histories[i]):] *= fitness_histories[i][-1]

        fitness_histories = temp
        fitness_histories = sorted(fitness_histories, key=lambda x: x[-1])
        fitness_min = fitness_histories[0]
        fitness_average = np.average(fitness_histories, axis=0)
        fitness_max = fitness_histories[-1]

        fitness_history = [nb_gen, fitness_min, fitness_average, fitness_max]

        return fitness_history

    def save_all(self):
        self.logger.info('-' * 50)
        self.save_info()
        self.save_csv()
        self.save_model()
        self.logger.info('-' * 50)

    def save_csv(self):
        os.makedirs(f'logs/{self.name}', exist_ok=True)
        fitness_history = np.transpose(self.fitness_history())
        np.savetxt(f'logs/{self.name}/{self.timestamp}_v{self.version}.csv',
                   fitness_history, fmt='%i', delimiter=',')

        self.logger.info(f'Saved logs/{self.name}/{self.timestamp}_v{self.version}.csv')

    def save_model(self):
        os.makedirs(f'logs/{self.name}', exist_ok=True)
        with open(f'logs/{self.name}/{self.timestamp}_v{self.version}.pkl', 'wb') as file:
            pkl.dump(self, file)
            self.logger.info(f'Saved logs/{self.name}/{self.timestamp}_v{self.version}.pkl')

    def save_info(self):
        os.makedirs(f'logs/{self.name}', exist_ok=True)
        with open(f'logs/{self.name}/{self.timestamp}_v{self.version}.txt', 'w') as file:
            file.write(f'nb_runs: {self.nb_runs}\r\n')

            file.write(f'MODEL\r\n')
            for k, v in self.model_config.items():
                file.write(f'{k}: {v}\r\n')

            file.write(f'INDIVIDUAL\r\n')
            for k, v in self.individual_config.items():
                file.write(f'{k}: {v}\r\n')

            file.write(f'GENETIC ALGORITHM\r\n')
            for k, v in self.genetic_algorithm_config.items():
                file.write(f'{k}: {v}\r\n')

            self.logger.info(f'Saved logs/{self.name}/{self.timestamp}_v{self.version}.txt')

    @staticmethod
    def load_model(path):
        with open(path, 'rb') as file:
            ttp_runner = pkl.load(file)
            ttp_runner.version += 1
            return ttp_runner

    def visualization(self):
        show_learning_plot(*self.fitness_history())
