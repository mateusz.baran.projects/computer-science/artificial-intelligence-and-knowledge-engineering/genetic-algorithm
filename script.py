from runners.ttp_runner import TTPRunner

if __name__ == '__main__':
    directory = r'/drive/Projects/computer-science/' \
                r'artificial-intelligence-and-knowledge-engineering/lab1/ai-lab1-ttp_data/'

    # file_name = 'trivial_1.ttp'
    # file_name = 'easy_0.ttp'
    # file_name = 'medium_0.ttp'
    file_name = 'hard_3.ttp'

    ttp_runner = TTPRunner(
        name=file_name,
        model_config={
            'path': directory + file_name,
        },
        individual_config={
            'greedy_algorithm': 'ratio',
        },
        genetic_algorithm_config={
            "population_size": 500,
            "nb_generations": 0,
            "tournament_size": 5,
            "ranking_size": 0,
            "reproduction_size": 300,
            "children_size": 300,
            "random_reproduction_size": 50,
            "mutation_probability": .05,
            "log_interval": 10,
            "visualization": False,
            "timeout": 20,
            "selection_type": 'tournament',
            "crossover_type": 'ox',
            "mutation_type": 'inverse',
        },
        nb_runs=3,
    )

    ttp_runner.run()
    ttp_runner.save_all()
    ttp_runner.visualization()
