from runners.ttp_runner import TTPRunner

if __name__ == '__main__':
    directory = r'/drive/Projects/computer-science/artificial-intelligence-' \
                r'and-knowledge-engineering/lab1/ttp-genetic-algorithm/logs'

    # file_name = 'trivial_1.ttp'
    # file_name = 'easy_0.ttp'
    # file_name = 'medium_0.ttp'
    model_name = 'hard_3.ttp'

    file_name = '15-03-2019_14-05-48_v1.pkl'

    ttp_runner = TTPRunner.load_model(f'{directory}/{model_name}/{file_name}')

    ttp_runner.run()
    ttp_runner.save_all()
    ttp_runner.visualization()
