import matplotlib.pyplot as plt


def show_route(individual):
    x = []
    y = []
    for feature in individual.genome:
        x.append(individual.model.nodes[feature]['X'])
        y.append(individual.model.nodes[feature]['Y'])

    plt.plot(x, y, x[0], y[0], 'og', x[-1], y[-1], 'or')
    plt.show()


def show_learning_plot(nb_generations, fitness_min, fitness_average, fitness_max):
    plt.plot(nb_generations, fitness_min, 'r')
    plt.plot(nb_generations, fitness_average, 'y')
    plt.plot(nb_generations, fitness_max, 'g')
    plt.show()
